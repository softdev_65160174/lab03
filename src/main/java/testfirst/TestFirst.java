/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package testfirst;

/**
 *
 * @author informatics
 */
public class TestFirst {

    static boolean checkWin(char[][] table, int row,char currentPlayer) {
        if(checkRow(table ,row ,currentPlayer)){
            return true;
        }
        if(checkCol(table ,row ,currentPlayer)){
            return true;
        }
        if(checkDiagonal(table)){
            return true;
        }
      return false;
    }

    private static boolean checkRow(char[][] table, int row, char currentPlayer) {
        for(int i = 0 ;i<3;i++){
            if(table[row-1][i] != currentPlayer){
                return false;
            }
        }
        return true;
    }

    private static boolean checkCol(char[][] table, int col, char currentPlayer) {
        for(int i = 0 ;i<3;i++){
            if(table[i][col-1] != currentPlayer){
                return false;
            }
        }
        return true;
    }

    private static boolean checkDiagonal(char[][] table) {
        if(table[0][0] == table[1][1] && table[1][1] == table[2][2] || table[0][2] == table[1][1] && table[1][1] == table[2][0]){
            return true;
        }
      return false;
    }
    
    public static boolean checkDraw(char[][] table) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (table[i][j] != 'X' && table[i][j] != 'O') {
                    return false;
                }
            }
        }
        return true;

    }


}
